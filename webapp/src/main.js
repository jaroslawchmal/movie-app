import { createApp, defineAsyncComponent } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Keycloak from "keycloak-js";
import "flowbite";
import BaseCard from "./components/ui/BaseCard.vue";
import ShowCard from "./components/ui/media/ShowCard.vue";
import BaseButton from "./components/ui/BaseButton.vue";
import BaseSpinner from "./components/ui/BaseSpinner.vue";
import BasePagination from "./components/ui/BasePagination.vue";

let initOptions = {
  url: process.env.VUE_APP_KEYCLOAK_URL,
  realm: process.env.VUE_APP_REALM,
  clientId: process.env.VUE_APP_CLIENT_ID,
  //onLoad: "login-required",
  onLoad: "check-sso",
  silentCheckSsoRedirectUri: window.location.origin + "/silent-check-sso.html",
};
let keycloak = Keycloak(initOptions);
keycloak
  .init({ onLoad: initOptions.onLoad }) //{ onLoad: initOptions.onLoad }
  .then((auth) => {
    if (auth) {
      console.log("authenticated");
    } else {
      console.log("unauthenticated");
    }
  })
  .catch(() => {
    console.log("Authenticated Failed");
  });

keycloak.onTokenExpired = () => {
  console.log("expired " + new Date());
  keycloak
    .updateToken(50)
    .then((refreshed) => {
      if (refreshed) {
        store.dispatch("auth/setUser", {
          userId: keycloak.tokenParsed.sub,
          username: keycloak.tokenParsed.preferred_username,
          name: keycloak.tokenParsed.name,
          token: keycloak.token,
        });
        console.log("refreshed " + new Date());
      } else {
        console.log("not refreshed " + new Date());
      }
    })
    .catch(() => {
      console.error("Failed to refresh token " + new Date());
      keycloak.clearToken();
    });
};
/*
keycloak.onReady = () => {
  if (keycloak.token) {
    store.dispatch("auth/setUser", {
      userId: keycloak.tokenParsed.sub,
      user: keycloak.tokenParsed.preferred_username,
      token: keycloak.token,
    });
  }
};
*/
keycloak.onAuthSuccess = () => {
  console.log("onAuthSuccess: " + keycloak.tokenParsed.preferred_username);
  console.log("parsed: " + JSON.stringify(keycloak.tokenParsed));
  store.dispatch("auth/setUser", {
    userId: keycloak.tokenParsed.sub,
    username: keycloak.tokenParsed.preferred_username,
    name: keycloak.tokenParsed.name,
    token: keycloak.token,
  });
};
keycloak.onAuthError = () => {
  store.dispatch("auth/logout");
  keycloak.clearToken();
};
keycloak.onAuthLogout = () => {
  store.dispatch("auth/logout");
  keycloak.clearToken();
};
keycloak.onAuthRefreshSuccess = () => {
  console.log(
    "onAuthRefreshSuccess: " + keycloak.tokenParsed.preferred_username
  );
  store.dispatch("auth/setUser", {
    userId: keycloak.tokenParsed.sub,
    username: keycloak.tokenParsed.preferred_username,
    name: keycloak.tokenParsed.name,
    token: keycloak.token,
  });
};
keycloak.onAuthRefreshError = () => {
  console.log("onAuthRefreshError: " + keycloak.tokenParsed.preferred_username);
  store.dispatch("auth/logout");
  keycloak.clearToken();
};

let app = createApp(App);
const BaseDialog = defineAsyncComponent(() =>
  import("./components/ui/BaseDialog.vue")
);
app.component("base-card", BaseCard);
app.component("base-button", BaseButton);
app.component("base-spinner", BaseSpinner);
app.component("base-dialog", BaseDialog);
app.component("base-pagination", BasePagination);
app.component("show-card", ShowCard);
app.use(store).use(router).mount("#app");

export default keycloak;
