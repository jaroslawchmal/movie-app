const config = {
    MOVIE_DB_API_KEY: process.env.VUE_APP_MOVIEDB_API_KEY,
  };
  
  export default config;