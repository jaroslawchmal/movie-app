import axios from "axios";
import config from "./config";

export const axiosMovieDB = axios.create({
  baseURL: "https://api.themoviedb.org/3",
  params: {
    api_key: config.MOVIE_DB_API_KEY,
    language: "en-US",
  },
});

export const axiosSpring = axios.create({
  baseURL: "http://localhost:9090/",
  params: {
    //spring config, token maybe?
  },
});


