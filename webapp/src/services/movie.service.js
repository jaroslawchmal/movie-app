import { axiosMovieDB as axios } from "./config/axios";

class MovieService {
  MOVIE_DB_IMAGE_URL = {
    small: "https://image.tmdb.org/t/p/w185",
    medium: "https://image.tmdb.org/t/p/w300",
    large: "https://image.tmdb.org/t/p/w1280",
    original: "https://image.tmdb.org/t/p/original",
  };

  getMovies = (requestData) => {
    const filter = requestData.filter || "popular";
    return axios.get(`/movie/${filter}`, {
      params: { page: requestData.page },
    });
  };
  searchMovies = (requestData) => {
    return axios.get(`/search/movie`, {
      params: { query: requestData.query, page: requestData.page },
    });
  };

  getGenres = () => {
    return axios.get("/genre/movie/list");
  };

  getMovie = (id) => {
    return axios.get(`/movie/${id}`);
  };

  getActors = (id) => {
    return axios.get(`/movie/${id}/credits`);
  };

  getMovieImages = (id) => {
    return axios.get(`/movie/${id}/images`, { params: { language: "null" } });
  };

  getRecommendations = (id) => {
    return axios.get(`/movie/${id}/recommendations`, {
      params: {
        language: "null",
        page: 1,
      },
    });
  };
}
export default new MovieService();
