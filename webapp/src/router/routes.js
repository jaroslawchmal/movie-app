import HomeView from "../views/HomeView.vue";
import NotFoundView from "../views/NotFoundView.vue";
export default [
  {
    path: "/",
    name: "home",
    redirect: { name: "movies", params: { filter: "popular", page: 1 } },
  },
  {
    path: "/movies/:filter/:page",
    name: "movies",
    props: (route) => ({
      filter: route.params.filter,
      page: Number(route.params.page),
    }),
    component: HomeView,
  },
  {
    path: "/movie/:id",
    name: "movie",
    props: true,
    component: HomeView,
  },
  {
    path: "/tvshows",
    name: "tvshows",
    component: HomeView,
  },

  {
    path: "/about",
    name: "about",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
  {
    path: "/watchlist",
    name: "watchlist",
    component: () =>
      import(/* webpackChunkName: "watchlist" */ "../views/WatchlistView.vue"),
  },
  { path: "/:notFound(.*)", component: NotFoundView },
];
