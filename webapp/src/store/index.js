import { createStore } from 'vuex'
import authModule from './modules/auth.js';
import moviesModule from './modules/movies.js';
export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth: authModule,
    movies: moviesModule,
  }
})
