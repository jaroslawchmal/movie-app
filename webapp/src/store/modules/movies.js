export default {
  namespaced: true,
  state() {
    return {
      genres: [],
    };
  },
  mutations: {
    setGenres(state, payload) {
      state.genres = payload;
    },
  },
  actions: {
    setGenres(context, payload) {
      context.commit("setGenres", payload);
    },
  },
  getters: {
    genres(state) {
      return state.genres;
    },
  },
};
