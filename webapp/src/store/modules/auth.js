import keycloak from "../../main.js";
export default {
  namespaced: true,
  state() {
    return {
      userId: null,
      username: null,
      name: null,
      token: null,
    };
  },
  mutations: {
    setUser(state, payload) {
      state.token = payload.token;
      state.username = payload.username;
      state.name = payload.name;
      state.userId = payload.userId;
    },
    /* 
    setAutoLogout(state) {
      state.didAutoLogout = true;
    }, */
  },
  actions: {
    setUser(context, payload) {
      localStorage.setItem("username", payload.username);
      context.commit("setUser", {
        ...payload,
      });
    },
    login() {
      keycloak.login();
    },
    tryLogin(context) {
      const username = localStorage.getItem("username");
      console.log("username: " + username);

      if (!username) {
        context.commit("setUser", {
          token: null,
          username: null,
          name: null,
          userId: null,
        });
      } else {
        context.commit("setUser", {
          userId: keycloak.tokenParsed.sub,
          username: keycloak.tokenParsed.preferred_username,
          name: keycloak.tokenParsed.name,
          token: keycloak.token,
        });
      }
    },
    logout(context) {
      localStorage.removeItem("username");
      keycloak.logout();
      context.commit("setUser", {
        userId: null,
        username: null,
        name: null,
        token: null,
      });
    },
  },
  getters: {
    userId(state) {
      return state.userId;
    },
    token(state) {
      return state.token;
    },
    username(state) {
      return state.username;
    },
    name(state) {
      return state.name;
    },
    isAuthenticated(state) {
      return !!state.token;
    },
  },
};
